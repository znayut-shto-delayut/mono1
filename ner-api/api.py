from fastapi.responses import JSONResponse
from fastapi import FastAPI, UploadFile, File, HTTPException
from ner import get_tags
import requests


app = FastAPI()


@app.post("/")
def compose_apis(text: str):
    return JSONResponse(get_tags(text))

