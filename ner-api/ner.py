import spacy
import re
import pymorphy2
import pandas as pd


class entity_collector:
    def __init__(self, text):
        self.text = self.preprocess_text(text)
        nlp = spacy.load("ru_core_news_lg")
        self.doc = nlp(self.text)

        self.personalities = pd.DataFrame()
        self.organizations = pd.DataFrame()
        self.locations = pd.DataFrame()

        self.norms = pd.DataFrame()
        self.type_collections = {
            "ORG": self.organizations,
            "LOC": self.locations,
            "PER": self.personalities
        }

        for name in ["ORG", "LOC", "PER"]:
            self.define_borders(name)

        self.entities = {
            "ORG": self.organizations["name"].to_list(),
            "LOC": self.locations["name"].to_list(),
            "PER": self.personalities["name"].to_list()
        }

    def preprocess_text(self, text):
        text = text.lower()
        text = re.sub(r'[^\w\s]', '', text)
        text = re.sub(r'[  ]', ' ', text)
        return text

    def define_borders(self, type):
        type_indices = []
        for index, token in enumerate(self.doc):
            if token.ent_type_ == type:
                type_indices += [index]
                # print(token.text, token.ent_type_, token)
        type_indices += ["-10"]

        left_borders = []
        right_borders = []

        # print("indices:", type_indices)
        # 0 4 5  6
        l = type_indices[0]
        for i in range(len(type_indices[:-1])):
            if type_indices[i] + 1 != type_indices[i + 1]:
                r = type_indices[i]
                left_borders += [l]
                right_borders += [r]

                l = type_indices[i + 1]

        self.type_collections[type]["l"] = left_borders
        self.type_collections[type]["r"] = right_borders
        text = self.text.split(' ')
        df = self.type_collections[type]
        self.type_collections[type]["name"] = [text[l:r + 1] for l, r in zip(df['l'], df['r'])]


def get_tags(text: str) -> dict:
    kek = entity_collector(text)
    people = open('new_people.txt', 'r').read().split('\n')
    morph = pymorphy2.MorphAnalyzer(lang='ru')
    for i in range(len(kek.entities['PER'])):
        for j in range(len(kek.entities['PER'][i])):
            kek.entities['PER'][i][j] = morph.parse(kek.entities['PER'][i][j])[0].normal_form
    persons = []
    for p in kek.entities['PER']:
        possible_persons = []
        part = p[0]
        for person in people:
            if part in person.lower().split():
                possible_persons.append(person)
        if len(p) > 1:
            for part in p[1:]:
                new_possible_persons = []
                for person in possible_persons:
                    if part in person.lower().split():
                        new_possible_persons.append(person)
                possible_persons = new_possible_persons

        if not possible_persons:
            new_person = []
            for part in p:
                new_person.append(part[0].upper() + part[1:])
            persons.append([' '.join(new_person)])
        else:
            add_person = True
            for i in range(len(persons)):
                persons_intersection = list(set(persons[i]) & set(possible_persons))
                if persons_intersection:
                    persons[i] = persons_intersection
                    add_person = False
                    break
            if add_person:
                persons.append(possible_persons)

    new_persons = []
    for person in persons:
        if len(person) == 1:
            new_persons.append(person)
        elif len(person) > 1:
            possible_persons = person
            surname = possible_persons[0].split()[0]
            f = True
            for pos in possible_persons:
                if surname != pos.split()[0]:
                    f = False
                    break
            if f:
                new_persons.append([surname[0].upper() + surname[1:]])
    kek.entities['PER'] = new_persons

    return kek.entities



if __name__ == "__main__":
	print(get_tags('В этом году на аукционе apple за $69,3 млн была продана цифровая картина Майка Винкельманна «Каждый день. Первые 5000 дней».'))

