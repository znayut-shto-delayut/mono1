# RBC 

Service for extracting text and keywords from video. Service extracts data from audio and visual streams of a video. Target input is RBC news video-records.

![](https://habrastorage.org/webt/lb/kk/eb/lbkkebdtnpeye3mukx4is_xhgws.png)

## Requirements

### Hardware

1. NVIDIA GPU

### Software

1. CUDA

2. Docker-desktop

3. Linux

## Usage

1. ``docker-compose up``

2. open http://localhost:80/

## Description

Service is a composite of several microservices for video-processing and GUI. First, `wav-slicer` with `speech-recognition` APIs recognize speech from audio stream. Second, `ner` API collect keywords from recognized text from audio. Third, `video-img2text` API extracts keywords from the picture stream. `general` API is a single endpoint for external services to use our services, it provides united data from all microservices. `keyword-extractor-stand` is a panel that provides user-interface of a service.

### Pipeline

1. User sends video to `keyword-extractor-stand` GUI

2. `keyword-extractor-stand` sends video to `general` API

3. `general` API sends video to `video-img2text` API and audio stream - to `wav-slicer` API

4. `video-img2text` API return to `general` API extracted keywords from picture stream

5. `wav-slicer` API makes preprocessing on audio stream: slices audio by long pauses and speaker changes

6. `wav-slicer` API notify `speech-recognition` API that slices are ready

7. `speech-recognition` API extracts text from speach of each slice and returns it to `general` API

8. `general` API sends recognized text to `ner` API

9. `ner` API detect keywords and returns them to `general` API

10. `general` API concantinate collected data and return them to `keyword-extractor-stand` GUI

![pipeline diagram](https://habrastorage.org/webt/xm/7v/05/xm7v053susuk79jx__qilqg7ybq.png)

### Microservices

#### `wav-slicer` API

Microservice that slice audio to optimize speech recognition. Slices audio by long pauses and speaker changes. Based on torch build-in functions.

#### `speech-recognition` API

Microservice that recognize speech from audio stream. Based on NVIDIA NEMO toolkit (sber golos).

#### `ner` API

Microservice that detect keywords from text. Based on Spacy neural network.

#### `keyword-extractor-stand`

User GUI that provides interface for video uploading and getting text and keywords json. Keywords are super-stars names, organizations and countries. Based on streamlit.

#### `video-img2text` API

Microservice that extracts keywords from picture stream of a video. Detects words with hashtags. Based on Tesseract for text recognition and EAST for text detection.

#### `general` API

Microservice is a single endpoint for external services to use our services, it provides united data from all microservices. Based on fastapi.

### Research

For voice recognition also was considered VOSK. It is not used since it is licenced and hard to learn.


