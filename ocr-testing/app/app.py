from PIL import Image
import pytesseract
from fastapi.responses import JSONResponse
from fastapi import FastAPI, File
import io


app = FastAPI()


@app.post("/")
def recognize_text_on_img(img_raw: bytes = File(...)):
    imageStream = io.BytesIO(img_raw)
    img = Image.open(imageStream)
    img.load()
    text = pytesseract.image_to_string(img, lang="rus")
    return JSONResponse({"text": text})
