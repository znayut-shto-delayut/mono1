# Tesseract based OCR
Tesseract recognise large russian text on image.

## Usage
``docker build -t api-ocr -f build/app/Dockerfile .``

``docker run -p 8080:8080 api-ocr``

```
import requests
with open("app/data/sammit-g20-rim.png", "rb") as f:
    img = f.read()
print(requests.post("http://0.0.0.0:8080/", files={"img_raw": img}).json())
```