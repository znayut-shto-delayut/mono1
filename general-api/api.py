import json
from uuid import uuid4
import os

from fastapi.middleware.wsgi import WSGIMiddleware
from fastapi.responses import JSONResponse
from fastapi import FastAPI, UploadFile, File, HTTPException

import requests

app = FastAPI()


@app.post("/")
def compose_apis(video_raw: UploadFile = File(...)):
    print(video_raw.filename)

    uuid = uuid4().hex
    os.mkdir(f'/{uuid}')

    file_name = f'/{uuid}/{video_raw.filename}'
    with open(file_name, 'wb+') as f:
        f.write(video_raw.file.read())

    wav_fn = f"{file_name}.wav"
    os.system(f"ffmpeg -i {file_name} -acodec pcm_s16le -ac 1 -ar 16000 {wav_fn} -y -af 'apad=pad_dur=10'")

    with open(wav_fn, "rb") as wav:
        sound_response = requests.post(f'http://wav-slicer:5000/upload/{uuid}',
                                     files={'wav_raw': wav.read()})

    with open(wav_fn, "rb") as wav:
        wav_response = requests.post(f'http://speech-recognition:5000/wav/',
                                     files={'wav_raw': wav.read()})

    with open(file_name, "rb") as vid:
        video_response = requests.post(f'http://video-img2text-api:8080/',
                                     files={'video_raw': vid.read()})

    ner = requests.post("http://ner-api:8080", params={"text": wav_response.json()[0]})

    return JSONResponse({
        "sound": {
            "vad_stt": sound_response.json(),
            'vad_stt_merged': ' '.join(sound_response.json()),
            "stt": wav_response.json()
        },
        "video": {
            "default": video_response.json()
        },
        "ner": ner.json()
    })
