# general-api

Text extractor api. Wrapper for other apis

## Usage

## Usage
``docker build -t api-general -f build/app/Dockerfile .``

``docker run -p 8080:8080 api-general``

```
import requests
print(requests.post("http://0.0.0.0:8080/", files={"video_raw": "frame 0, frame 1..."}).json())
```

