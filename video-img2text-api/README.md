# video-img2text-api

Extracts hashtag words from picture stream of video.

## Usage

## Usage
``docker build -t vidimgapi -f Dockerfile .``

``docker run -p 8080:8080 vidimgapi``

```
import requests
with open("7.mp4", "rb") as f:
    vid = f.read()
print(requests.post("http://0.0.0.0:8080/", files={"video_raw": ("7.mp4", vid)}).json())
```

