import requests
import os
import glob

from pathlib import Path
from fastapi.middleware.wsgi import WSGIMiddleware
from fastapi.responses import JSONResponse
from fastapi import FastAPI, UploadFile, File, HTTPException
from helper import video2text
from config import framerate


temp_path = "./temp_video"
Path(temp_path).mkdir(parents=True, exist_ok=True)


app = FastAPI()


@app.post("/")
def compose_apis(video_raw: UploadFile = File(...)):
    # clean everything before continue
    files = glob.glob(f'{temp_path}/*')
    for f in files:
        os.remove(f)

    # save file as file
    file_name = f"{temp_path}/{video_raw.filename}"
    with open(file_name, 'wb+') as f:
        f.write(video_raw.file.read())

    recognized_text = list(video2text(file_name, framerate))
    return JSONResponse({"recognized_text": recognized_text})
