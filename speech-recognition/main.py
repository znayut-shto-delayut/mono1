from uuid import uuid4
import os
import torch
import nemo.collections.asr as nemo_asr
import uvicorn
from fastapi import FastAPI, File, UploadFile

def absoluteFilePaths(directory):
    for dirpath,_,filenames in os.walk(directory):
        for f in filenames:
            yield os.path.abspath(os.path.join(dirpath, f))

def infer_beam_search_lm(files, asr_model, beam_search_lm):
    hyps = []
    logits = torch.tensor(asr_model.transcribe(files, batch_size=20, logprobs=True))
    log_probs_length = torch.tensor([logit.shape[0] for logit in logits])
    logits_tensor = torch.nn.utils.rnn.pad_sequence(logits, batch_first=True)
    for j in range(logits_tensor.shape[0]):
        best_hyp = beam_search_lm.forward(log_probs=logits_tensor[j].unsqueeze(0),
                                          log_probs_length=log_probs_length[j].unsqueeze(0))[0][0][1]
        hyps.append(best_hyp)
    return hyps


def infer_greedy(files, asr_model):
    transcripts = asr_model.transcribe(paths2audio_files=files, batch_size=20)
    return transcripts


asr_model = nemo_asr.models.EncDecCTCModel.restore_from('/workspace/models/QuartzNet15x5_golos.nemo')

beam_search_lm = nemo_asr.modules.BeamSearchDecoderWithLM(
    vocab=list(asr_model.decoder.vocabulary),
    beam_width=16,
    alpha=2, beta=1.5,
    lm_path='/workspace/models/kenlms/lm_commoncrawl.binary',
    num_cpus=1,
    cutoff_prob=1.0, cutoff_top_n=40,
    input_tensor=True)


app = FastAPI()


@app.post("/wav/")
async def image(wav_raw: UploadFile = File(...)):
    print(wav_raw.file)
    try:
        os.mkdir("images")
        print(os.getcwd())
    except Exception as e:
        print(e)
    file_name = os.getcwd() + "/images/" + uuid4().hex + '.wav'
    with open(file_name, 'wb+') as f:
        f.write(wav_raw.file.read())
    hyps_lm = infer_beam_search_lm([file_name], asr_model, beam_search_lm)

    return hyps_lm


@app.post("/upload/{item_id}")
async def image(item_id: str):
    hyps = []
    files = sorted(absoluteFilePaths(f'/exchange/{item_id}'), key=lambda x: int(x.split('/')[-1].split('.')[0]))
    for e in files:
        hyps += infer_beam_search_lm([e], asr_model, beam_search_lm)

    return hyps

uvicorn.run(app, host="0.0.0.0", port=5000, log_level="info")