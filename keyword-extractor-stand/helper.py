import requests
import shutil
import magic
import uuid
import time
import os

repo_banner_file = os.path.abspath("./eah.svg")


class UI:
    css = f"""
        <style>
            .reportview-container .main .block-container{{
                max-width: 1200px;
                padding-top: 2rem;
                padding-right: 2rem;
                padding-left: 2rem;
                padding-bottom: 2rem;
            }}
            .reportview-container .main {{
                color: "#111";
                background-color: "#eee";
            }}
        </style>
    """


headers = {"Content-Type": "application/json"}


def search(endpoint, filename="query.png"):
    """search_by_file.

    :param endpoint:
    :param filename:
    """
    filename = os.path.abspath(filename)

    with open(filename, "rb") as file:
        image = file.read()

    files = {"image": image}

    response = requests.post(endpoint, files=files)
    response = response.json()
    if "result" not in response:
        return []
    return response["result"]


def create_temp_file(query, output_file_dir="/tmp"):
    """create_temp_file.

    :param output_file_dir:
    :param query:
    """

    is_old = time.time() - (15 * 60)
    for i in os.listdir(output_file_dir):
        path = os.path.join(output_file_dir, i)
        if os.stat(path).st_mtime <= is_old:
            if os.path.isfile(path):
                try:
                    os.remove(path)
                except:
                    print("Could not remove file:", i)

    data = query.read()

    ext = query.name.split(".")[-1]
    path = os.path.join(output_file_dir, str(uuid.uuid4()) + "." + ext)

    with open(path, "wb") as file:
        file.write(data)

    return path
