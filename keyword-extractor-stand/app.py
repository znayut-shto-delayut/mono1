from helper import UI, create_temp_file
import streamlit as st
import requests


matches = None

# Layout
st.set_page_config(page_title="video2text", layout="wide")
st.markdown(
    body=UI.css,
    unsafe_allow_html=True,
)

# text on page
st.header("Загрузите видео")

# drag n drop and preview areas
upload_cell, preview_cell = st.columns([12, 1])
# get uploaded file
allowed_extensions = ["mpg", "mov", "wmv", "avi", "mp4", "wav"]
query = upload_cell.file_uploader("", type=allowed_extensions)

# validate file
if query:
    # save as temp file
    uploaded_video = create_temp_file(query)
    with open(uploaded_video, "rb") as video:
        # when button click
        if st.button(label="Распознать"):
            # check if file is uploaded
            if not query:
                # alert
                st.markdown("Пожалуйста, загрузите видео!")
            else:
                # make file request
                video_raw = video.read()
                api_response = requests.post('http://general-api:8080/', files={'video_raw': ('input.' + uploaded_video.split('.')[-1], video_raw)}).text
                # draw response
                st.json(api_response)
