import os
import glob
import torch
torch.set_num_threads(1)
import uvicorn
from fastapi import FastAPI, File, UploadFile
from pathlib import Path
import requests
from config import notification_message, notification_address


device = torch.device("cpu")
model, utils = torch.hub.load(repo_or_dir='snakers4/silero-vad',
                              model='silero_vad',
                              force_reload=True, device=device)
(get_speech_ts, _, save_audio, read_audio, _, _, _) = utils


app = FastAPI()


@app.post("/upload/{item_id}")
async def wav_slice(item_id: str, wav_raw: UploadFile = File(...)):
    os.mkdir(f"/exchange/{item_id}")

    file_name = "/tmp/uploaded.wav"
    with open(file_name, 'wb+') as f:
        f.write(wav_raw.file.read())

    wav = read_audio(file_name)
    speech_timestamps = get_speech_ts(wav, model,
                                      num_steps=4)
    for n, i in enumerate(speech_timestamps):
        save_audio(f'/exchange/{item_id}/{n}.wav', wav[i['start']: i['end']], 16000)

    os.remove(file_name)

    api_response = requests.post(f'http://speech-recognition:5000/upload/{item_id}').json()

    return api_response

uvicorn.run(app, host="0.0.0.0", port=5000, log_level="info")
